<?php ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Job posting</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TCXN336');</script>
    <!-- End Google Tag Manager -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="/libs/normalize.css/normalize.css">
    <link rel="stylesheet" href="/libs/jquery-nice-select/css/nice-select.css">
    <link rel="stylesheet" href="/libs/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/libs/multi-select/jquery.multiselect.css">
    <link rel="stylesheet" href="/libs/fancybox/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/libs/air-datepicker/dist/css/datepicker.min.css">
    <link rel="stylesheet" href="/libs/scrollbar/jquery.scrollbar.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/site.css">
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token"
          content="u-UWI_RDLw6ug7eYnSvT_eM5x9DP5MZFT8-5CTkyDhDurC9TkyVGXevQ6PmpbrqFmXaUgqO-sggV94peVWE6Yg==">

    <link href="/css/site.css" rel="stylesheet">
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCXN336"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="header-relative ">
    <div class="j-header header-fixed">
        <header class="header header--employer">
            <div class="container">
                <div class="row">
                    <div class="header__col">
                        <a href="/" class="header__logo">
                            <img src="/img/global/work_people_logo_003.png" alt="">
                        </a>
                        <ul class="header__menu">
                        </ul>
                    </div>
                    <div class="header__border"></div>
                    <div class="header__col header__col--2">
                        <a href="/userpanel/vacancy/create" class="header__add">
                            <span></span>
                            <div>Post Vacancy</div>
                        </a>
                        <a href="/site/for-candidate" class="header__login">
                            For candidates </a>
                        <a href="/site/login" class="header__login">
                            Login </a>

                        <select name="" class="change-translation-widget j-select header__select select">
                            <option value="/en/userpanel/service/vip" selected>English</option>
                            <option value="/ru/userpanel/service/vip" 1>Рус.</option>
                            <option value="/ua/userpanel/service/vip" 1>Укр.</option>
                        </select>

                    </div>
                    <div class="header__mobile-row">
                        <div class="info__menu">
                            <div class="info__zvon-wrap j-zvon-wrapper">
                                <a href="/userpanel/vacancy/create" class="header__add header-add-mobile-guest">
                                    <span></span>
                                    <div>Post Vacancy</div>
                                </a>
                            </div>
                        </div>
                        <div class="toggle-burger j-toggle">
                            <div>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="toggle-menu j-toggle-menu">
                <div class="toggle-menu__top">
                    <div class="toggle-menu__row">

                        <select name="" class="change-translation-widget j-select header__select select">
                            <option value="/en/userpanel/service/vip" selected>English</option>
                            <option value="/ru/userpanel/service/vip" 1>Рус.</option>
                            <option value="/ua/userpanel/service/vip" 1>Укр.</option>
                        </select>

                        <div class="toggle-menu__close j-toggle-close">
                            <div>
                                Close <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="header__menu">
                    <li>
                        <a href="/site/login">
                            <div class="avatar">
                                <div>Login</div>
                            </div>
                        </a>
                    </li>
                </ul>
                <ul class="header__menu header__menu--grey">
                    <li>
                        <a href="/resume/search">Find resume</a>
                    </li>
                    <li>
                        <a href="/site/for-candidate">
                            For candidates </a>
                    </li>
                </ul>
            </div>
        </header>
    </div>
</div>

<div class="loading" id="loaderWait" style="display:none;">Loading&#8230;</div>
<div class="breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs__list">
            <li><a href="/">Home</a></li>
            <li><a href="/userpanel">User panel</a></li>
            <li><a href="/userpanel/service/index">Services and accounts</a></li>
            <li><a href="/userpanel/service/price">Services and prices</a></li>
            <li class="active">Job posting</li>
        </ul>
    </div>
</div>


<div class="container">
    <div class="title-sec">
        Job posting
    </div>
    <div class="title-desc">
        Your vacancy will be placed on the first page of the site. And as a Gift, you will be placed in the TOP
        vacancies in your category when searching. The number of job views will increase 10-12 times. Thousands of job
        seekers looking for work will see her.
    </div>
</div>
<div id="appTarif" class="buy-services buy-services--publication">
    <div class="container">
        <div class="row info-tarif_row">
            <div class="col">
                <form id="w0" action="/userpanel/service/accept" method="post">
                    <input type="hidden" name="_csrf"
                           value="u-UWI_RDLw6ug7eYnSvT_eM5x9DP5MZFT8-5CTkyDhDurC9TkyVGXevQ6PmpbrqFmXaUgqO-sggV94peVWE6Yg==">
                    <div class="trud-if__bl ">
                        <div class="trud-if__select">
                            <select class="select" name="Tarif[id]" id="select_group_10">
                                <option value="100">
                                    1 публикация
                                </option>
                                <option value="110" selected>
                                    3 публикация
                                </option>
                                <option value="120">
                                    10 публикация
                                </option>
                            </select>
                        </div>
                        <div class="trud-if__title">
                            Package №1
                            <div class="trud-if__title-description">
                                (suitable for small sets)
                            </div>
                        </div>
                        <hr>
                        <div class="trud-if__list" id="tarif_10_100" style="display:none;">
                            <div>
                                Top announcement for 7 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                upvote in list <br>
                            </div>
                            <hr>
                            <div>
                                VIP ad
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-final-price">200</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                        <div class="trud-if__list" id="tarif_10_110">
                            <div>
                                Top announcement for 7 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                upvote in list <br>
                            </div>
                            <hr>
                            <div>
                                VIP ad
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-discount-price">550</div>
                                    <div class="tarif-discount-size">
                                        <div class="tarif-discount-marker">-30%</div>
                                    </div>
                                    <div class="tarif-final-price">385</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                        <div class="trud-if__list" id="tarif_10_120" style="display:none;">
                            <div>
                                Top announcement for 7 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                upvote in list <br>
                            </div>
                            <hr>
                            <div>
                                VIP ad
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-discount-price">1900</div>
                                    <div class="tarif-discount-size">
                                        <div class="tarif-discount-marker">-40%</div>
                                    </div>
                                    <div class="tarif-final-price">1140</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col">
                <form id="w1" action="/userpanel/service/accept" method="post">
                    <input type="hidden" name="_csrf"
                           value="u-UWI_RDLw6ug7eYnSvT_eM5x9DP5MZFT8-5CTkyDhDurC9TkyVGXevQ6PmpbrqFmXaUgqO-sggV94peVWE6Yg==">
                    <div class="trud-if__bl ">
                        <div class="trud-if__select">
                            <select class="select" name="Tarif[id]" id="select_group_20">
                                <option value="200">
                                    1 публикация
                                </option>
                                <option value="210" selected>
                                    3 публикация
                                </option>
                                <option value="220">
                                    10 публикация
                                </option>
                            </select>
                        </div>
                        <div class="trud-if__title">
                            Package №2
                            <div class="trud-if__title-description">
                                <span class="tarif_more_green">13 x more</span> views
                            </div>
                        </div>
                        <hr>
                        <div class="trud-if__list" id="tarif_20_200" style="display:none;">
                            <div>
                                Top announcement for 14 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                one upvote in list <br>
                                (upvote every 3 days)
                            </div>
                            <hr>
                            <div>
                                VIP ad
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-final-price">400</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                        <div class="trud-if__list" id="tarif_20_210">
                            <div>
                                Top announcement for 14 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                3 upvotes in list <br>
                                (upvote every 3 days)
                            </div>
                            <hr>
                            <div>
                                VIP ad
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-discount-price">1200</div>
                                    <div class="tarif-discount-size">
                                        <div class="tarif-discount-marker">-43%</div>
                                    </div>
                                    <div class="tarif-final-price">684</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                        <div class="trud-if__list" id="tarif_20_220" style="display:none;">
                            <div>
                                Top announcement for 14 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                7 upvotes in list <br>
                                (upvote every 3 days)
                            </div>
                            <hr>
                            <div>
                                VIP ad
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-discount-price">4000</div>
                                    <div class="tarif-discount-size">
                                        <div class="tarif-discount-marker">-50%</div>
                                    </div>
                                    <div class="tarif-final-price">2000</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col">
                <form id="w2" action="/userpanel/service/accept" method="post">
                    <input type="hidden" name="_csrf"
                           value="u-UWI_RDLw6ug7eYnSvT_eM5x9DP5MZFT8-5CTkyDhDurC9TkyVGXevQ6PmpbrqFmXaUgqO-sggV94peVWE6Yg==">
                    <div class="trud-if__bl trud-if__bl--yellow">
                        <div class="trud-if__select">
                            <select class="select" name="Tarif[id]" id="select_group_30">
                                <option value="300">
                                    1 публикация
                                </option>
                                <option value="310" selected>
                                    3 публикация
                                </option>
                                <option value="320">
                                    10 публикация
                                </option>
                            </select>
                        </div>
                        <div class="trud-if__title">
                            Package №3
                            <div class="trud-if__title-description">
                                <span class="tarif_more_green">30 x more</span> views
                            </div>
                        </div>
                        <hr>
                        <div class="trud-if__list" id="tarif_30_300" style="display:none;">
                            <div>
                                Top announcement for 30 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                3 upvotes in list <br>
                                (upvote every 3 days)
                            </div>
                            <hr>
                            <div>
                                VIP ad on home page on 7 days
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-final-price">550</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                        <div class="trud-if__list" id="tarif_30_310">
                            <div>
                                Top announcement for 30 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                9 upvotes in list <br>
                                (upvote every 3 days)
                            </div>
                            <hr>
                            <div>
                                VIP ad on home page on 7 days
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-discount-price">1600</div>
                                    <div class="tarif-discount-size">
                                        <div class="tarif-discount-marker">-40%</div>
                                    </div>
                                    <div class="tarif-final-price">960</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                        <div class="trud-if__list" id="tarif_30_320" style="display:none;">
                            <div>
                                Top announcement for 30 days + publication in the general list (published at the top of
                                the list and highlighted)
                            </div>
                            <hr>
                            <div>
                                20 upvotes in list <br>
                                (upvote every 3 days)
                            </div>
                            <hr>
                            <div>
                                VIP ad on home page on 7 days
                                <br>
                                (Home ad)
                            </div>
                            <hr>
                            <div class="tarif-block-price">
                                <div>
                                    <div class="tarif-discount-price">5400</div>
                                    <div class="tarif-discount-size">
                                        <div class="tarif-discount-marker">-50%</div>
                                    </div>
                                    <div class="tarif-final-price">2700</div>
                                </div>
                                <div class="tarif-block-price__currency">
                                    UAH
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn--green">Select</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="title-sec">
            You will be seen first on the home page
        </div>
        <div class="vip-publication">
            <div class="vip-publication__col">
                <div class="vip-publication__screen">
                    Скрин сайта
                </div>
            </div>
            <div class="vip-publication__col">
                <div class="vip-publication__bl">
                    <div class="vip-publication__zag">
                        VIP - объявления
                    </div>
                    <div class="vip-publication__item">
                        <div class="vip-publication__img">
                            <a href="single.php">
                                <img src="/img/vacancy/1.jpg" alt="">
                            </a>
                        </div>
                        <div class="vip-publication__main">
                            <a href="single.php" class="vip-publication__title">
                                Разнорабочие на автомобильный завод
                            </a>
                            <div class="vip-publication__info">
                                <ul class="vacancy__list">
                                    <li>
                                        <div class="vacancy__list-img">
                                            <img src="/img/vacancy/list3.png" alt="">
                                        </div>
                                        Город:
                                        <div class="vacancy__city">Чехия, Прага</div>
                                    </li>
                                    <li>
                                        <div class="vacancy__list-img">
                                            <img src="/img/vacancy/list1.png" alt="">
                                        </div>
                                        Работодатель: <a href="#">dinelgroup</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="vip-publication__item">
                        <div class="vip-publication__img">
                            <a href="single.php">
                                <img src="/img/vacancy/1.jpg" alt="">
                            </a>
                        </div>
                        <div class="vip-publication__main">
                            <a href="single.php" class="vip-publication__title">
                                Разнорабочие на автомобильный завод
                            </a>
                            <div class="vip-publication__info">
                                <ul class="vacancy__list">
                                    <li>
                                        <div class="vacancy__list-img">
                                            <img src="/img/vacancy/list3.png" alt="">
                                        </div>
                                        Город:
                                        <div class="vacancy__city">Чехия, Прага</div>
                                    </li>
                                    <li>
                                        <div class="vacancy__list-img">
                                            <img src="/img/vacancy/list1.png" alt="">
                                        </div>
                                        Работодатель: <a href="#">dinelgroup</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="vip-publication__item">
                        <div class="vip-publication__img">
                            <a href="single.php">
                                <img src="/img/vacancy/1.jpg" alt="">
                            </a>
                        </div>
                        <div class="vip-publication__main">
                            <a href="single.php" class="vip-publication__title">
                                Разнорабочие на автомобильный завод
                            </a>
                            <div class="vip-publication__info">
                                <ul class="vacancy__list">
                                    <li>
                                        <div class="vacancy__list-img">
                                            <img src="/img/vacancy/list3.png" alt="">
                                        </div>
                                        Город:
                                        <div class="vacancy__city">Чехия, Прага</div>
                                    </li>
                                    <li>
                                        <div class="vacancy__list-img">
                                            <img src="/img/vacancy/list1.png" alt="">
                                        </div>
                                        Работодатель: <a href="#">dinelgroup</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__logo footer__col">
                <img src="/img/global/work_people_logo_003_white.png" alt="">
            </div>
            <div class="footer__col">
                <div class="footer__title">
                    About us
                </div>
                <ul class="footer__list">
                    <li><a href="/jobs">All jobs</a></li>
                    <li><a href="/company-reviews">Company reviews</a></li>
                </ul>
            </div>
            <div class="footer__col">
                <div class="footer__title">
                    How it works?
                </div>
                <ul class="footer__list">
                    <li><a href="/userpanel/vacancy/create">Post a resume</a></li>
                    <li><a href="#">For employers</a></li>
                </ul>
            </div>
            <div class="footer__col">
                <div class="footer__title">
                    Help
                </div>
                <ul class="footer__list">
                    <li><a href="/site/about">About the project</a></li>
                    <li><a href="/site/contacts">Contacts</a></li>
                    <li><a href="/site/faq">FAQ</a></li>
                    <li><a href="/site/terms-of-use">Terms of use</a></li>
                    <li><a href="/site/paid-services">Paid services</a></li>
                    <li><a href="/site/map">Site map</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="/libs/jquery/dist/jquery.min.js"></script>
<script src="/libs/underscore/underscore-min.js"></script>
<script src="/libs/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="/libs/slick-carousel/slick/slick.min.js"></script>
<script src="/libs/multi-select/jquery.multiselect.js"></script>
<script src="/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/libs/scrollmagic/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="/libs/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script src="/libs/air-datepicker/dist/js/datepicker.min.js"></script>
<script src="/libs/img-preview/jquery.uploadPreview.min.js"></script>
<script src="/libs/scrollbar/jquery.scrollbar.min.js"></script>
<script src="/libs/ckeditor5/translations/ru.js"></script>
<script src="/libs/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script src="/js/common.js"></script>
<script src="/assets/20aa8e00/yii.js"></script>
<script src="/assets/20aa8e00/yii.activeForm.js"></script>
<script>jQuery(function ($) {
        jQuery('#w0').yiiActiveForm([], []);
        jQuery('#w1').yiiActiveForm([], []);
        jQuery('#w2').yiiActiveForm([], []);
        $(document).ready(function () {
            let tarif_type_list = [
                10,
                20,
                30,
            ];

            for (let i = 0; i < tarif_type_list.length; i++) {
                let select_item = $('#select_group_' + tarif_type_list[i]);
                select_item.niceSelect();
                select_item.on('change', function (e) {
                    // hide all
                    let el = $(this).find('option').each(function () {
                        $('#tarif_' + tarif_type_list[i] + '_' + $(this).val()).hide();
                    });

                    // show selected
                    $('#tarif_' + tarif_type_list[i] + '_' + $(this).val()).show();
                });
            }

        });
        $(document).ready(function () {
            $(".change-translation-widget").change(function () {
                document.location.href = $(this).val();
            });
        });
    });</script>
</body>
</html>


