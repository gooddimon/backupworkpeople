<?php ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Services and prices</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TCXN336');</script>
    <!-- End Google Tag Manager -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" href="/libs/normalize.css/normalize.css">
    <link rel="stylesheet" href="/libs/jquery-nice-select/css/nice-select.css">
    <link rel="stylesheet" href="/libs/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/libs/multi-select/jquery.multiselect.css">
    <link rel="stylesheet" href="/libs/fancybox/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/libs/air-datepicker/dist/css/datepicker.min.css">
    <link rel="stylesheet" href="/libs/scrollbar/jquery.scrollbar.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/site.css">    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="33FvHHgwaHn2YVLqe1d3QbXA_h8pK6qExcKZy4qCcbGKOFZsH1YBKrMyDYtPEh45z4-tTUVx3smf-qqc5tFFww==">

    <link href="/css/site.css" rel="stylesheet"></head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCXN336"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="header-relative ">
    <div class="j-header header-fixed">
        <header class="header header--employer">
            <div class="container">
                <div class="row">
                    <div class="header__col">
                        <a href="/" class="header__logo">
                            <img src="/img/global/work_people_logo_003.png" alt="">
                        </a>
                        <ul class="header__menu">
                        </ul>
                    </div>
                    <div class="header__border"></div>
                    <div class="header__col header__col--2">
                        <a href="/userpanel/vacancy/create" class="header__add">
                            <span></span> <div>Post Vacancy</div>
                        </a>
                        <a href="/site/for-candidate" class="header__login">
                            For candidates                                    </a>
                        <a href="/site/login" class="header__login">
                            Login                                </a>

                        <select name="" class="change-translation-widget j-select header__select select">
                            <option value="/en/userpanel/service/price" selected >English</option>
                            <option value="/ru/userpanel/service/price" 1 >Рус.</option>
                            <option value="/ua/userpanel/service/price" 1 >Укр.</option>
                        </select>

                    </div>
                    <div class="header__mobile-row">
                        <div class="info__menu">
                            <div class="info__zvon-wrap j-zvon-wrapper">
                                <a href="/userpanel/vacancy/create" class="header__add header-add-mobile-guest">
                                    <span></span> <div>Post Vacancy</div>
                                </a>
                            </div>
                        </div>
                        <div class="toggle-burger j-toggle">
                            <div>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="toggle-menu j-toggle-menu">
                <div class="toggle-menu__top">
                    <div class="toggle-menu__row">

                        <select name="" class="change-translation-widget j-select header__select select">
                            <option value="/en/userpanel/service/price" selected >English</option>
                            <option value="/ru/userpanel/service/price" 1 >Рус.</option>
                            <option value="/ua/userpanel/service/price" 1 >Укр.</option>
                        </select>

                        <div class="toggle-menu__close j-toggle-close">
                            <div>
                                Close                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="header__menu">
                    <li>
                        <a href="/site/login">
                            <div class="avatar">
                                <div>Login</div>
                            </div>
                        </a>
                    </li>
                </ul>
                <ul class="header__menu header__menu--grey">
                    <li>
                        <a href="/resume/search">Find resume</a>
                    </li>
                    <li>
                        <a href="/site/for-candidate">
                            For candidates                                    </a>
                    </li>
                </ul>
            </div>
        </header>
    </div>
</div>

<div class="loading" id="loaderWait" style="display:none;">Loading&#8230;</div>
<div class="breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs__list"><li><a href="/">Home</a></li>
            <li><a href="/userpanel">User panel</a></li>
            <li><a href="/userpanel/service/index">Services and accounts</a></li>
            <li class="active">Services and prices</li>
        </ul>    </div>
</div>


<div class="info-company">
    <div class="container">
        <div class="title-sec">
            Services and prices		</div>
        <div class="row info-company__row j-trigger">
            <div class="col">
                <hr style="margin: 0">
                <div class="services-prices">
                    <div class="services-prices__item j-edit" id="bl1">
                        <div class="services-prices__img">
                            <div class="services-prices__img-inner">
                                <img src="/img/services-prices/1.png" alt="">
                            </div>
                        </div>
                        <div class="services-prices__main">
                            <div class="services-prices__title">
                                VIP job postings							</div>
                            <p>
                                You will be placed in the TOP vacancies in your category when searching. The number of job views will increase 5-7 times.							</p>
                            <a href="/userpanel/service/vip" class="btn btn--trans-yellow">More</a>
                        </div>
                        <div class="services-prices__price">
                            <div>
                                from <b>200</b> UAH							</div>
                            <div>
                                for <b>1</b> pc							</div>
                        </div>
                    </div>
                    <div class="services-prices__item j-edit" id="bl3">
                        <div class="services-prices__img">
                            <div class="services-prices__img-inner">
                                <img src="/img/services-prices/3.png" alt="">
                            </div>
                        </div>
                        <div class="services-prices__main">
                            <div class="services-prices__title">
                                Индивидуальный найм персонала
                            </div>
                            <p>

                            </p>
                            <a href="/userpanel/service/individual" class="btn btn--trans-yellow">Подробнее</a>
                        </div>
                        <div class="services-prices__price">
                            <div>
                                from <b> 500</b> UAH							</div>
                            <div>
                                <b>1</b> работник
                            </div>
                        </div>
                    </div>
                    <div class="services-prices__item j-edit" id="bl4">
                        <div class="services-prices__img">
                            <div class="services-prices__img-inner">
                                <img src="/img/services-prices/4.png" alt="">
                            </div>
                        </div>
                        <div class="services-prices__main">
                            <div class="services-prices__title">
                                Доступ к базе анкет
                            </div>
                            <p>

                            </p>
                            <a href="#" class="btn btn--trans-yellow">Подробнее</a>
                        </div>
                        <div class="services-prices__price">
                            <div>
                                <b>Бесплатно</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col info-company__col j-height-sticky-column">
                <div class="sidebar j-sticky">
                    <div class="sidebar__title">
                        Адрес страницы вашей компании на сайте
                    </div>
                    <div class="sidebar__content">
                        <ul class="sidebar__list">
                            <li><a href="#bl1" class="j-scroll"><b>VIP</b> публикации вакансий</a></li>
                            <!-- <li><a href="#bl2" class="j-scroll"><b>TOP</b> публикации вакансий</a></li> -->
                            <li><a href="#bl3" class="j-scroll">Индивидуальный набор</a></li>
                            <li><a href="#bl4" class="j-scroll">Доступ к базе анкет</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__logo footer__col">
                <img src="/img/global/work_people_logo_003_white.png" alt="">
            </div>
            <div class="footer__col">
                <div class="footer__title">
                    About us				</div>
                <ul class="footer__list">
                    <li><a href="/jobs">All jobs</a></li>
                    <li><a href="/company-reviews">Company reviews</a></li>
                </ul>
            </div>
            <div class="footer__col">
                <div class="footer__title">
                    How it works?				</div>
                <ul class="footer__list">
                    <li><a href="/userpanel/vacancy/create">Post a resume</a></li>
                    <li><a href="#">For employers</a></li>
                </ul>
            </div>
            <div class="footer__col">
                <div class="footer__title">
                    Help				</div>
                <ul class="footer__list">
                    <li><a href="/site/about">About the project</a></li>
                    <li><a href="/site/contacts">Contacts</a></li>
                    <li><a href="/site/faq">FAQ</a></li>
                    <li><a href="/site/terms-of-use">Terms of use</a></li>
                    <li><a href="/site/paid-services">Paid services</a></li>
                    <li><a href="/site/map">Site map</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="/libs/jquery/dist/jquery.min.js"></script>
<script src="/libs/underscore/underscore-min.js"></script>
<script src="/libs/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="/libs/slick-carousel/slick/slick.min.js"></script>
<script src="/libs/multi-select/jquery.multiselect.js"></script>
<script src="/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/libs/scrollmagic/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="/libs/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script src="/libs/air-datepicker/dist/js/datepicker.min.js"></script>
<script src="/libs/img-preview/jquery.uploadPreview.min.js"></script>
<script src="/libs/scrollbar/jquery.scrollbar.min.js"></script>
<script src="/libs/ckeditor5/translations/ru.js"></script>
<script src="/libs/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script src="/js/common.js"></script>
<script src="/assets/20aa8e00/yii.js"></script>
<script>jQuery(function ($) {
        $(document).ready(function() {
            $( ".change-translation-widget" ).change(function() {
                document.location.href = $( this ).val();
            });
        });
    });</script></body>
</html>


