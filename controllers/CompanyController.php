<?php

namespace app\controllers;

use app\components\CurrencyConverterHelper;
use app\models\CategoryVacancy;
use app\models\UploadExcelForm;
use app\models\User;
use app\models\UserPhone;
use app\models\Vacancy;
use app\models\VacancyImage;
use Yii;
use app\models\Company;
use app\models\CompanySearch;
use app\models\VacancySearch;
use yii\helpers\BaseFileHelper;
use yii\imagine\Image as Imagine;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionImport()
    {
        $model = new UploadExcelForm();

        if (Yii::$app->request->isPost) {
            $post_data = Yii::$app->request->post();

            if (!empty($post_data['file_path'])) {
                $model->file_path = $post_data['file_path']; //! unsafe data need filter

                // save uploaded file
                $data_arr = $model->processExcel();
                $parse_result = $this->processData($data_arr, true);
            } else {
                $model->excel_file = UploadedFile::getInstance($model, 'excel_file');
                if (!$model->upload()) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return [
                        'success' => false,
                        'errors' => $model->getErrors()
                    ];
                }

                // process uploaded file
                $data_arr = $model->processExcel();
                $parse_result = $this->processData($data_arr, false);
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'success' => true,
                'data' => [
                    'file_path' => $model->file_path,
                    'parse_result' => $parse_result,
                ]
            ];
        }

        // import data from excel file
        return $this->render('import', [
            'model' => $model
        ]);
    }

    protected function processData($data_arr, $save_data)
    {
        $parse_result = [];

        if (!empty($data_arr[0])) {
            $headers = $data_arr[0];
            $model_attribute_index = [];

            // compare header names with model data
            foreach ($headers as $index => $header) {
                $attribute_name = $this->getAttributeNameByLabel($header);
                if ($attribute_name !== null) {
                    $model_attribute_index[$index] = $attribute_name;
                }
            }

            $model_data_list = [];

            $saveDirectoryTmp = Yii::getAlias(Company::TMP_IMAGE_DIR_ALIAS);
            if (!BaseFileHelper::createDirectory($saveDirectoryTmp, $mode = 0775)) {
                throw new HttpException(500, Yii::t('vacancy', 'Не могу создать каталог на сервере: ' . $saveDirectoryTmp));
            }

            foreach ($data_arr as $i => $item) {
                if ($i == 0) continue; // skip headers
                $model_data = [];

                foreach ($model_attribute_index as $index => $attribute) {
                    if (!isset($item[$index])) { // skip empty fields
                        continue;
                    }

                    switch ($attribute) {
                        case 'user_d':
                            $model_data['Vacancy'][$attribute] = $item[$index];
                        case 'employment_type':
                            $model_data['Vacancy'][$attribute] = $this->getEmploymentTypeByLabel($item[$index]);
                            break;
                        case 'creation_time':
                            $model_data['Vacancy'][$attribute] = Yii::$app->formatter->asTimestamp($item[$index]);
                            break;
                        // case 'salary_per_hour_min':
                        case 'gender_list':
                            $model_data['Vacancy'][$attribute] = $this->getGendersByLabel($item[$index]);
                            break;
                        case 'vacancy_company_name':
                            $model_data['Vacancy']['company_name'] = $item[$index];
                            break;
                        case 'date_free':
                            $model_data['Vacancy']['date_free'] = $this->getDateFreeByLabel($item[$index]);
                            break;
                        case 'date_start':
                            $model_data['Vacancy']['date_start'] = date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]));
                            break;
                        case 'date_end':
                            $model_data['Vacancy']['date_end'] = date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]));
                            break;
                        case 'worker_country_codes':
                            $model_data['Vacancy']['worker_country_codes'] = $this->getCountryCodesByLabel($item[$index]);
                            break;
                        case 'residence_provided':
                            $model_data['Vacancy']['residence_provided'] = $this->getResidenceProvidedByLabel($item[$index]);
                            break;
                        case 'documents_provided':
                            $model_data['Vacancy']['documents_provided'] = $this->getDocumentsProvidedByLabel($item[$index]);
                            break;
                        case 'documents_required':
                            $model_data['Vacancy']['documents_required'] = $this->getDocumentsRequiredByLabel($item[$index]);
                            break;
                        case 'contact_email_list':
                            $contact_email_list = explode(',', $item[$index]);
                            foreach ($contact_email_list as &$email) {
                                $email = trim($email);
                            }
                            unset($email);

                            if (!empty($contact_email_list)) {
                                $model_data['Vacancy']['contact_email_list'] = implode($contact_email_list, ';');
                            }
                            break;
                        case 'contact_phone':
                            $contact_phone = explode(',', $item[$index]);
                            foreach ($contact_phone as &$phone) {
                                $phone = trim($phone);
                            }
                            unset($phone);

                            if (!empty($contact_phone)) {
                                $model_data['Vacancy']['contact_phone'] = implode($contact_phone, ';');
                            }
                            break;
                        // relations:
                        // -- company
                        case 'company_phone':
                            $model_data['Company']['company_phone'] = '' . $item[$index];
                            break;
                        case 'company_name':
                            $model_data['Company']['company_name'] = $item[$index];
                            break;
                        case 'company_email':
                            $model_data['Company']['company_email'] = $item[$index];
                            break;
                        case 'company_verification':
                            $model_data['Company']['status'] = $this->getCompanyStatusByLabel($item[$index]);
                            break;
                        case 'company_worker_name':
                            $model_data['UserPhone']['company_worker_name'] = $item[$index];
                            break;
                        case 'company_worker_email':
                            $model_data['UserPhone']['company_worker_email'] = $item[$index];
                            break;
                        case 'contact_phone_for_admin':
                            $model_data['UserPhone']['contact_phone_for_admin'] = '' . $item[$index];
                            break;
                        case 'country_name':
                            $model_data['Vacancy'][$attribute] = $this->getCountryCodeByLabel($item[$index]);
                            break;
                        case 'company_country_code':
                            $model_data['Company']['company_country_code'] = $this->getCountryCodeByLabel($item[$index]);
                            break;
                        case 'company_country_city_id':
                            $model_data['Vacancy'][$attribute] = $this->getCityIdByLabel($item[$index]);
                            $model_data['Company']['company_country_city_id'] = $model_data['Vacancy'][$attribute];
                            break;
                        case 'type':
                            $model_data['Company']['type'] = $this->getCompanyTypeByLabel($item[$index]);
                            break;
                        case 'logo':
                            $model_data['Company']['logo_src'] = $item[$index]; // will load from remote server
                            break;
                        case 'source_url':
                            $model_data['Vacancy']['source_url'] = substr($item[$index], 0, 1000);
                            break;
                        // -- other relations
                        case 'categoryVacancies':
                            $category_id = $this->getCategoryByLabel($item[$index]);
                            if ($category_id !== null) {
                                $model_data['Vacancy']['categoryVacancies'][] = ['category_id' => $category_id];
                            }
                            break;
                        case 'category_job_id':
                            $model_data['Vacancy'][$attribute] = $this->getCategoryJobByLabel($item[$index]);
                            if (empty($model_data['Vacancy'][$attribute])) {
                                $model_data['Vacancy'][$attribute] = "5";
                            }
                            break;
                        // --
                        default:
                            if (!empty($item[$index])) {
                                $model_data['Vacancy'][$attribute] = $item[$index];
                            }
                            break;
                    }
                }

                // if `gender_list` not filled by default setup 'male' and 'female'
                if (empty($model_data['Vacancy']['gender_list'])) {
                    $model_data['Vacancy']['gender_list'] = Vacancy::GENDER_MALE . ';' . Vacancy::GENDER_FEMALE . ';';
                }

                // if `company_country_code` not filled use Vacancy country_name
                if (empty($model_data['Company']['company_country_code']) && !empty($model_data['Vacancy']['country_name'])) {
                    $model_data['Company']['company_country_code'] = $model_data['Vacancy']['country_name'];
                }

                // if `country_name` not filled use Company company_country_code
                if (empty($model_data['Vacancy']['country_name']) && !empty($model_data['Company']['company_country_code'])) {
                    $model_data['Vacancy']['country_name'] = $model_data['Company']['company_country_code'];
                }

                // if empty company phone, use vacancy `contact_phone`
                if (empty($model_data['Company']['company_phone'])) {
                    $model_data['Company']['company_phone'] = '' . $model_data['Vacancy']['contact_phone'];
                }

                // if empty company type, default employer
                if (empty($model_data['Company']['type'])) {
                    $model_data['Company']['type'] = Company::TYPE_EMPLOYER;
                }

                // if `worker_country_codes` empty setup current cantry code
                if (empty($model_data['Vacancy']['worker_country_codes']) && !empty($model_data['Vacancy']['country_name'])) {
                    $model_data['Vacancy']['worker_country_codes'] = $model_data['Vacancy']['country_name'] . ';';
                }

                // setup `creation_time`
                if (empty($model_data['Vacancy']['creation_time'])) {
                    $model_data['Vacancy']['creation_time'] = time();
                }

                $model_data['Vacancy']['upvote_time'] = $model_data['Vacancy']['creation_time'];
                $model_data['Vacancy']['update_time'] = $model_data['Vacancy']['creation_time'];

                // convert currency to search
                if (!empty($model_data['Vacancy']['salary_per_hour_min']) && !empty($model_data['Vacancy']['currency_code'])) {
                    $model_data['Vacancy']['salary_per_hour_min_src'] = CurrencyConverterHelper::currencyToCurrency(
                        $model_data['Vacancy']['salary_per_hour_min'],
                        $model_data['Vacancy']['currency_code'],
                        Yii::$app->params['sourceCurrencyCharCode']
                    );
                }

                if (empty($model_data['Vacancy']['salary_per_hour_min_src'])) {
                    $model_data['Vacancy']['salary_per_hour_min_src'] = "5";
                }
                if (empty($model_data['Vacancy']['salary_per_hour_min'])) {
                    $model_data['Vacancy']['salary_per_hour_min'] = "5";
                }

                // convert currency to search
                if (!empty($model_data['Vacancy']['salary_per_hour_max']) && !empty($model_data['Vacancy']['currency_code'])) {
                    $model_data['Vacancy']['salary_per_hour_max_src'] = CurrencyConverterHelper::currencyToCurrency(
                        $model_data['Vacancy']['salary_per_hour_max'],
                        $model_data['Vacancy']['currency_code'],
                        Yii::$app->params['sourceCurrencyCharCode']
                    );
                }

                // if `hours_per_day_min` setup default by field `employment_type`
                if (empty($model_data['Vacancy']['hours_per_day_min'])
                    && !empty($model_data['Vacancy']['employment_type'])
                    && $model_data['Vacancy']['employment_type'] == Vacancy::EMPLOYMENT_TYPE_FULL_TIME
                ) {
                    $model_data['Vacancy']['hours_per_day_min'] = '8'; // hours
                }

                // if `days_per_week_min` setup default by field `employment_type`
                if (empty($model_data['Vacancy']['days_per_week_min'])) {
                    $model_data['Vacancy']['days_per_week_min'] = '5'; // days
                }
                if (empty($model_data['Vacancy']['days_per_week_min']) && !empty($model_data['Vacancy']['employment_type'])) {
                    $model_data['Vacancy']['days_per_week_min'] = '5'; // days
                }

                if (empty($model_data['Vacancy']['type_of_working_shift'])) {
                    $model_data['Vacancy']['type_of_working_shift'] = Vacancy::TYPE_OF_WORKING_SHIFT_DAY . ';' . Vacancy::TYPE_OF_WORKING_SHIFT_EVENING . ';';
                }

                // clean description
                if (empty($model_data['Vacancy']['full_import_description_cleaned']) && !empty($model_data['Vacancy']['full_import_description'])) {
                    //! BUG, need sanitize html code
                    $model_data['Vacancy']['full_import_description_cleaned'] = $model_data['Vacancy']['full_import_description'];
                }

                if (empty($model_data['Vacancy']['job_description']) && !empty($model_data['Vacancy']['full_import_description_cleaned'])) {
                    $model_data['Vacancy']['job_description'] = trim(preg_replace('/\s+/', ' ', strip_tags($model_data['Vacancy']['full_import_description_cleaned'])));
                }

                // !BUG, empty data
                if (empty($model_data['Vacancy']['contact_email_list'])) {
                    $model_data['Vacancy']['contact_email_list'] = '-';
                }

                // !BUG, empty data
                if (empty($model_data['Vacancy']['residence_provided'])) {
                    $model_data['Vacancy']['residence_provided'] = Vacancy::RESIDENCE_PROVIDED_NO;
                }

                $model_data_list[] = $model_data;
            }

            // upload images from remove server
            //? setup timeout and memory limit
            if ($save_data) {
                $chs = array();
                $cmh = curl_multi_init();
                for ($t = 0; $t < count($model_data_list); $t++) {
                    if (empty($model_data_list[$t]['Company']['logo_src'])) {
                        continue;
                    }

                    // setup tmp file path
                    $tmp = explode('.', $model_data_list[$t]['Company']['logo_src']);
                    $file_ext = array_pop($tmp);
                    $tmp = explode('/', implode('.', $tmp));
                    $file_name = array_pop($tmp);
                    $model_data_list[$t]['Company']['logo_tmp_path'] = $saveDirectoryTmp . DIRECTORY_SEPARATOR . md5($model_data_list[$t]['Company']['logo_src']) . '.' . $file_ext;
                    $model_data_list[$t]['Vacancy']['vacancyImages'] = [
                        [
                            'photo_tmp_path' => $model_data_list[$t]['Company']['logo_tmp_path'],
                            'name' => $file_name,
                        ]
                    ];

                    // check is file already uploaded
                    if (file_exists($model_data_list[$t]['Company']['logo_tmp_path'])) {
                        continue; // file already uploaded
                    }

                    $chs[$t] = curl_init();
                    curl_setopt($chs[$t], CURLOPT_URL, $model_data_list[$t]['Company']['logo_src']);
                    curl_setopt($chs[$t], CURLOPT_RETURNTRANSFER, 1);
                    curl_multi_add_handle($cmh, $chs[$t]);
                }

                $running = null;
                do {
                    curl_multi_exec($cmh, $running);
                } while ($running > 0);

                for ($t = 0; $t < count($model_data_list); $t++) {
                    if (empty($chs[$t])) {
                        continue;
                    }

                    try { // supress file 404 errors
                        $tmp_file_name = tempnam(sys_get_temp_dir(), 'VC_IMG');
                        file_put_contents($tmp_file_name, curl_multi_getcontent($chs[$t]));
                        $data = file_get_contents($tmp_file_name);
                        $imagine = Imagine::getImagine();
                        $image = $imagine->load($data);
                        Imagine::resize($image, 800, 600)
                            ->save($model_data_list[$t]['Company']['logo_tmp_path'], ['quality' => 90]);

                        curl_multi_remove_handle($cmh, $chs[$t]);
                        curl_close($chs[$t]);
                    } catch (\Throwable $e) {
                        unset($model_data_list[$t]['Company']['logo_src']);
                        unset($model_data_list[$t]['Company']['logo_tmp_path']);
                        unset($model_data_list[$t]['Vacancy']['vacancyImages']);
                    }
                }
                curl_multi_close($cmh);
            }
            // --

            // process data to build models
            foreach ($model_data_list as $row_index => $model_data) {
                //! BUG, need register all phone numbers, - get only first phone number,
                $contact_phone = null;
                if (!empty($model_data['Vacancy']['contact_phone'])) {
                    $contact_phone_list = explode(';', $model_data['Vacancy']['contact_phone']);
                    $contact_phone = array_shift($contact_phone_list);
                }

                // try to find company id by phone number
                if (empty($contact_phone)) {
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => [[
                            'Can\'t create vacancy without contact phone'
                        ]],
                        'data' => $model_data
                    ];
                    continue; // go next row
                }

                $user_phone_model = UserPhone::find()->where(['phone' => $contact_phone])->one();
                if (empty($user_phone_model)) { // account not exists
                    // create new account and company
                    $model_data['User'] = [
                        'login' => $contact_phone,
                        'username' => $contact_phone,
                        'email' => $contact_phone . '@unknown.host',
                    ];

                    $model_data['UserPhone'] = [
                        'verified' => UserPhone::VERIFIED_INSERTED_BY_PARSER,
                        'phone' => $contact_phone,
                        'company_worker_name' => empty($model_data['UserPhone']['company_worker_name']) ? null : $model_data['UserPhone']['company_worker_name'],
                        'company_worker_email' => empty($model_data['UserPhone']['company_worker_email']) ? null : $model_data['UserPhone']['company_worker_email'],
                        'contact_phone_for_admin' => empty($model_data['UserPhone']['contact_phone_for_admin']) ? null : $model_data['UserPhone']['contact_phone_for_admin'],
                    ];
                } else if (!empty($user_phone_model->user->profile)) { // this is worker's account
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => [[
                            'Not acceptable. Phone number ' . $contact_phone . ' already registred as worker and can\'t create Vacancy'
                        ]],
                        'data' => $model_data
                    ];
                    continue; // go next row
                } else { // company founded and exists
                    $company_model = $user_phone_model->user->company;
                    if (empty($company_model)) { // user do not complete registration, just create company for this account
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => [[
                                'Not acceptable. Phone number ' . $contact_phone . ' already registred user do not complete registration and can\'t create Vacancy'
                            ]],
                            'data' => $model_data
                        ];
                        continue; // go next row
                    }

                    $model_data['Vacancy']['company_id'] = $company_model->id;
                    $model_data['Vacancy']['user_phone_id'] = $user_phone_model->id;
                }

                $model_item = new Vacancy();

                // save all data into DB
                $db = $model_item->getDb();
                $trans = $db->beginTransaction();

                if (empty($model_data['Vacancy']['user_phone_id'])) { // || empty($model_data['Vacancy']['company_id'])
                    // lets create new account
                    $model_company = new Company();
                    $model_user = new User();
                    $model_user_phone = new UserPhone();

                    $model_company->loadDefaultValues();
                    $model_user->loadDefaultValues();
                    $model_user_phone->loadDefaultValues();

                    $model_company->load($model_data);
                    $model_user->load($model_data);
                    $model_user_phone->load($model_data);

                    if (!$model_user->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_user->getErrors(),
                            'data' => $model_user->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_company->user_id = $model_user->id;
                    if (!$model_company->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_company->getErrors(),
                            'data' => $model_company->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_user_phone->user_id = $model_user->id;
                    if (!$model_user_phone->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_user_phone->getErrors(),
                            'data' => $model_user_phone->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_data['Vacancy']['company_id'] = $model_company->id;
                    $model_data['Vacancy']['user_phone_id'] = $model_user_phone->id;
                }

                // laod all data
                $model_item->loadDefaultValues();
                $model_item->load($model_data);

                // validate model
                if ($model_item->save()) {
                    if (!empty($model_data['Vacancy']['categoryVacancies'])) {
                        foreach ($model_data['Vacancy']['categoryVacancies'] as $categoryVacancy) {
                            $model_category_vacancy = new CategoryVacancy();
                            $model_category_vacancy->vacancy_id = $model_item->id;
                            $model_category_vacancy->category_id = $categoryVacancy['category_id'];
                            $model_category_vacancy->save();
                        }
                    }

                    if ($save_data) {
                        if (!empty($model_data['Vacancy']['vacancyImages'])) {
                            foreach ($model_data['Vacancy']['vacancyImages'] as $vacancy_image_data) {
                                $vacancy_image_data['vacancy_id'] = $model_item->id;
                                $model_vacancy_image = new VacancyImage();
                                $model_vacancy_image->load(['VacancyImage' => $vacancy_image_data]);
                                $model_vacancy_image->save();
                            }
                        }
                        $trans->commit();
                    } else {
                        $trans->rollBack();
                    }

                    $parse_result[$row_index] = [
                        'success' => true,
                        'data' => $model_item->getAttributes(), // $model_item->getAttributesWithRelatedAsPost()
                    ];
                } else {
                    $trans->rollBack();
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => $model_item->getErrors(),
                        'data' => $model_item->getAttributes()
                    ];
                }
            }
        }

        return $parse_result;
    }

    public function actionVacancy($id)
    {
        $searchModel = new VacancySearch();
        $searchModel->salary_currency = Yii::$app->params['defaultCurrencyCharCode'];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['company_id' => $id]);

        return $this->render('company', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('company', 'The requested page does not exist.'));
    }
}
