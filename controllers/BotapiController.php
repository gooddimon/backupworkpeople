<?php

namespace app\controllers;

use Yii;

use app\models\TelegramID;
use app\models\Vacancy;
use app\models\User;
use yii\db\Query;
use yii\web\Controller;

/**
 * VacancyController implements the CRUD actions for Vacancy model.
 */
class BotapiController extends Controller
{
    /**
     * Lists all Vacancy models.
     * @return mixed
     */
    public function actionOffers()
    {
        // vacancies_stack //

        $payload = "";
        $data["offers"] = [];
        $vacansy = Vacancy::find()->asArray()->all();
        foreach ($vacansy as $item) {
            array_push($data["offers"], $item);
        }
        $payload = json_encode($data);

        $ch = curl_init('https://api.pipe.bot/bot?apikey=e7e6e136690285708860712863150e19&vacancies_stack=');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload))
        );
        $result = curl_exec($ch);
        curl_close($ch);
        // vacancies_stack //

        //   Preferences   //

        if (isset($_GET["VacancySearch"])) {
            $VacancySearch = $_GET["VacancySearch"];
            $data = $VacancySearch;
            $preferences = [];
            $category_job_list_id = 0;
            $documents_requiredid = 0;

            foreach ($data["category_job_list"] as $id) {
                $category_job_list_id .= "," . $id;
            }

            foreach ($data["documents_required"] as $id) {
                $documents_requiredid .= " " . $id;
            }

            $category_job_list = $category_job_list_id;
            $gender_list = $data["gender_list"];
            $country_name = $data["country_name"];
            $country_city_id = $data["country_city_id"];
            $category_list = $data["category_list"];
            $salary_per_hour_min = $data["salary_per_hour_min"];
            $salary_per_hour_max = $data["salary_per_hour_max"];
            $residence_provided = $data["residence_provided"];
            $worker_country_codes = $data["worker_country_codes"];
            $documents_required = $documents_requiredid;

            $id = 0;

            $preferences["category"]["id"] = empty($category_job_list) ? 0 : $id;
            $preferences["category"]["name"] = empty($category_list) ? 0 : $category_list;
            $preferences["proffesion"]["id"] = empty($preferences["proffesion"]["id"]) ? 0 : $preferences["proffesion"]["id"];
            $preferences["proffesion"]["name"] = empty($preferences["proffesion"]["name"]) ? 0 : $preferences["proffesion"]["name"];
            $preferences["country"]["id"] = empty($preferences["country"]["id"]) ? 0 : $preferences["country"]["id"];
            $preferences["country"]["name"] = empty($preferences["country"]["name"]) ? 0 : $preferences["country"]["name"];
            $preferences["city"]["id"] = empty($preferences["city"]["id"]) ? 0 : $preferences["city"]["id"];
            $preferences["city"]["name"] = empty($preferences["city"]["name"]) ? 0 : $preferences["city"]["id"];
            $preferences["employment"]["id"] = empty($preferences["employment"]["id"]) ? 0 : $preferences["employment"]["id"];
            $preferences["employment"]["name"] = empty($preferences["employment"]["name"]) ? 0 : $preferences["employment"]["name"];
            $preferences["schedule"]["id"] = empty($preferences["schedule"]["id"]) ? 0 : $preferences["schedule"]["id"];
            $preferences["schedule"]["name"] = empty($preferences["schedule"]["name"]) ? 0 : $preferences["schedule"]["name"];
            $preferences["documents"]["id"] = empty($preferences["documents"]["id"]) ? 0 : $preferences["documents"]["id"];
            $preferences["documents"]["name"] = empty($preferences["documents"]["name"]) ? 0 : $preferences["documents"]["name"];
            $preferences["gender"]["id"] = empty($preferences["gender"]["id"]) ? 0 : $preferences["gender"]["id"];
            $preferences["gender"]["name"] = empty($preferences["gender"]["name"]) ? 0 : $preferences["gender"]["name"];
            $preferences["housing"]["id"] = empty($preferences["housing"]["id"]) ? 0 : $preferences["housing"]["id"];
            $preferences["housing"]["name"] = empty($preferences["housing"]["name"]) ? 0 : $preferences["housing"]["name"];
            $preferences["salaryusdfrom"] = empty($preferences["salaryusdfrom"]) ? 0 : $preferences["salaryusdfrom"];

            $ch = curl_init('https://api.pipe.bot/bot?apikey=e7e6e136690285708860712863150e19&vacancies_stack=');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($payload))
            );

            $result = curl_exec($ch);
            curl_close($ch);
            // vacancies_stack //

            $TelegramID = new TelegramID();
            $TelegramID->userpreferences = json_encode($preferences);
            $TelegramID->save();
            $this->redirect("https://pipe.bot/link?bot=5753");
        }

        if (isset($_GET["telegramid"])) {

            $preferencess = ['{
            "category": {
                "id": 2,
                "name": "Автомобильная промышленность"
            },
            "profession": [{
                "id": 25,
                "name": "Буфетчица"
            }, {
                "id": 20,
                "name": "Повар"
            }],
            "country": {
                "id": 1,
                "name": "Польша"
            },
            "сity": [
                {
                    "id": 1,
                    "name": "Ольштын"
                },
                {
                    "id": 2,
                    "name": "Варшава"
                },
                {
                    "id": 3,
                    "name": "Лодзь"
                }
            ],
            "employment": {
                "id": 10,
                "name": "Полная занятость"
            },
            "schedule": {
                "id": 20,
                "name": "Ночные"
            },
            "documents": [
                {
                    "id": 10,
                    "name": "Биометрический паспорт"
                },
                {
                    "id": 40,
                    "name": "ПМЖ"
                }
            ],
            "gender": {
                "id": 1,
                "name": "Мужской"
            },
            "housing": {
                "id": 2,
                "name": "Платное Жилье"
            },
            "salaryusdfrom": 1000
        }'];

            $id = $_GET["telegramid"];
            $TelID = new TelegramID();

            //      $result = $TelID->find()->where(["id" => (int)$id])->all();

            $result = $preferencess;

            return json_encode($result);
        }
        if (empty($_GET["telegramid"])) {
            echo "<span style='visibility: hidden'></span>";
        }
    }

    public function BotCatchIDUser()
    {
        $_GET["userID"];
        curl_init();

        curl_close();
    }
}
