<?php

namespace app\modules\Payments;

/**
 * admin module definition class
 */

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Payments\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->layout = '@app/modules/Payments/views/layouts/main';
        // custom initialization code goes here
    }
}
