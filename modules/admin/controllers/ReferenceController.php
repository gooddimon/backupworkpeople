<?php

namespace app\modules\admin\controllers;

use app\models\Profile;
use app\models\Resume;
use app\models\UploadExcelForm;
use app\models\UserPhone;
use Yii;
use app\models\User;
use yii\helpers\BaseFileHelper;
use yii\imagine\Image as Imagine;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\ReferenceHelper;
use app\components\CurrencyConverterHelper;
use yii\web\UploadedFile;

class ReferenceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            // ---
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [ // add access control by `role`
                    'class' => 'app\components\AccessRule'
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'import'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMINISTRATOR],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $reference_name = $params['file'];
        if (empty($reference_name)) {
            throw new NotFoundHttpException('Param `file` required.');
        }

        if (Yii::$app->request->isPost) {
            $post_data = Yii::$app->request->post();
            $data = [];
            foreach ($post_data['Reference'] as $val) {
                $data[] = $val;
            }

            if ($this->setReferenceInfo($reference_name, $data)) {
                if (Yii::$app->request->isAjax) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return [
                        'success' => true
                    ];
                }
            }
        }

        return $this->render('index', $this->getReferenceInfo($reference_name));
    }

    public function actionImport()
    {
        $model = new UploadExcelForm();

        if (Yii::$app->request->isPost) {
            $post_data = Yii::$app->request->post();

            if (!empty($post_data['file_path'])) {
                $model->file_path = $post_data['file_path']; //! unsafe data need filter

                // save uploaded file
                $data_arr = $model->processExcel();
                $parse_result = $this->processData($data_arr, true);
            } else {
                $model->excel_file = UploadedFile::getInstance($model, 'excel_file');
                if (!$model->upload()) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return [
                        'success' => false,
                        'errors' => $model->getErrors()
                    ];
                }

                // process uploaded file
                $data_arr = $model->processExcel();
                $parse_result = $this->processData($data_arr, false);
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'success' => true,
                'data' => [
                    'file_path' => $model->file_path,
                    'parse_result' => $parse_result,
                ]
            ];
        }

        // import data from excel file
        return $this->render('import', [
            'model' => $model
        ]);
    }

    protected function processData($data_arr, $save_data)
    {
        $parse_result = [];

        if (!empty($data_arr[0])) {
            $headers = $data_arr[0];
            $model_attribute_index = [];

            // compare header names with model data
            foreach ($headers as $index => $header) {
                $attribute_name = $this->getAttributeNameByLabel($header);
                if ($attribute_name !== null) {
                    $model_attribute_index[$index] = $attribute_name;
                }
            }

            $model_data_list = [];

            $saveDirectoryTmp = Yii::getAlias(Resume::TMP_IMAGE_DIR_ALIAS);
            if (!BaseFileHelper::createDirectory($saveDirectoryTmp, $mode = 0775)) {
                throw new HttpException(500, Yii::t('vacancy', 'Не могу создать каталог на сервере: ' . $saveDirectoryTmp));
            }

            foreach ($data_arr as $i => $item) {
                if ($i == 0) continue; // skip headers
                $model_data = [];

                foreach ($model_attribute_index as $index => $attribute) {
                    if (!isset($item[$index])) { // skip empty fields
                        continue;
                    }

                    switch ($attribute) {
                        case 'creation_time':
                            $model_data['Resume'][$attribute] = empty(date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]))) ? "null" : Yii::$app->formatter->asTimestamp($item[$index]);
                            break;
                        case 'update_time':
                            $model_data['Resume'][$attribute] = empty(date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]))) ? "null" : Yii::$app->formatter->asTimestamp($item[$index]);
                            break;
                        case 'birth_day':
                            $model_data['Resume'][$attribute] = empty(date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]))) ? "null" : Yii::$app->formatter->asTimestamp($item[$index]);
                            break;
                        case 'country_city_id':
                            $model_data['Resume'][$attribute] = $this->getCityIdByLabel($item[$index]);
                            break;
                        case 'country_name':
                            $model_data['Resume'][$attribute] = $this->getCountryCodeByLabel($item[$index]);
                            break;
                        case 'desired_country_of_work':
                            $country_list = $this->getCountryCodesByLabel($item[$index]);
                            if (!empty($country_list)) {
                                $model_data['Resume'][$attribute] = $country_list;
                            }
                            break;
                        case 'phone':
                            $contact_phone = explode(',', $item[$index]);
                            foreach ($contact_phone as &$phone) {
                                $phone = trim($phone);
                            }
                            unset($phone);

                            if (!empty($contact_phone)) {
                                $model_data['Resume'][$attribute] = implode($contact_phone, ';');
                            }
                            break;
                        case 'relocation_possible':
                            if ($item[$index] == 'возможен переезд') {
                                $model_data['Resume'][$attribute] = Resume::RELOCATION_POSSIBLE_YES;
                            } // else default value 20
                            break;
                        case 'photo_path':
                            $model_data['Resume']['photo_path_src'] = $item[$index]; // will load from remote server
                            break;
                        case 'gender_list':
                            $model_data['Resume'][$attribute] = $this->getGendersByLabel($item[$index]);
                            break;
                        // -- relations:
                        case 'resumeLanguages':
                            $model_data['Resume']['language'] = $item[$index];
                            $model_data['Resume']['use_language'] = Resume::USE_LANGUAGE_YES;
                            $model_data['Resume']['resumeLanguages'] = $this->getResumeLanguagesByLabel($item[$index]);

                            // if data recognized then use relation
                            if (!empty($model_data['Resume']['resumeLanguages'])) {
                                $model_data['Resume']['use_language'] = Resume::USE_LANGUAGE_REALTION;
                            }
                            break;
                        case 'resumeEducations':
                            // filer html
                            $model_data['Resume']['resumeEducations'][] = [
                                'description' => str_replace("_x000D_", " ", $item[$index])
                            ];
                            break;
                        case 'resumeJobs':
                            $model_data['Resume']['job_experience'] = empty($item[$index]) ? null : str_replace("_x000D_", " ", $item[$index]);
                            $model_data['Resume']['use_job_experience'] = $model_data['Resume']['full_import_description'];
                            break;
                        case 'title':
                            $model_data['Resume']['title'] = $item[$index];
                            if (empty($model_data['Resume']['use_title'])) { // first cycle
                                $model_data['Resume']['use_title'] = Resume::USE_TITLE_YES;
                            }
                        //!!! not break;
                        case 'resumeCategoryJobs':
                            $job_list = explode(',', $item[$index]);
                            foreach ($job_list as $job_label) {
                                $model_data['Resume']['resumeCategoryJobs'][] = [
                                    'category_job_id' => $this->getCategoryJobByLabel($job_label)
                                ];
                            }

                            // if data recognized then use relation
                            if (!empty($model_data['Resume']['resumeCategoryJobs'])) {
                                $model_data['Resume']['use_title'] = Resume::USE_TITLE_REALTION;
                            }
                            break;
                        case 'categoryResumes':
                            $category_id = $this->getCategoryByLabel($item[$index]);
                            if ($category_id !== null) {
                                $model_data['Resume']['categoryVacancies'] = ['category_id' => $category_id];
                            }
                            break;
                        default:
                            $model_data['Resume'][$attribute] = $item[$index];
                            break;
                    }
                }

                // clean description
                if (empty($model_data['Resume']['full_import_description_cleaned'])) {
                    //! BUG, need sanitize html code
                    $model_data['Resume']['full_import_description_cleaned'] = $model_data['Resume']['full_import_description'];
                }

                // clear duplicates resumeCategoryJobs
                if (!empty($model_data['Resume']['resumeCategoryJobs'])) {
                    $resumeCategoryJobs = [];
                    foreach ($model_data['Resume']['resumeCategoryJobs'] as $item) {
                        $funded = false;
                        foreach ($resumeCategoryJobs as $selected_item) {
                            if ($selected_item['category_job_id'] == $item['category_job_id']) {
                                $funded = true;
                            }
                        }

                        if (!$funded) {
                            $resumeCategoryJobs[] = $item;
                        }
                    }
                    $model_data['Resume']['resumeCategoryJobs'] = $resumeCategoryJobs;
                }

                // !BUG, empty data
                if (empty($model_data['Resume']['email'])) {
                    $model_data['Resume']['email'] = '-';
                }

                $model_data_list[] = $model_data;
            }

            // upload images from remove server
            //? setup timeout and memory limit
            if ($save_data) {
                $chs = array();
                $cmh = curl_multi_init();
                for ($t = 0; $t < count($model_data_list); $t++) {
                    if (empty($model_data_list[$t]['Resume']['photo_path_src'])) {
                        continue;
                    }

                    // setup tmp file path
                    $tmp = explode('.', $model_data_list[$t]['Resume']['photo_path_src']);
                    $file_ext = array_pop($tmp);
                    $tmp = explode('/', implode('.', $tmp));
                    $file_name = array_pop($tmp);
                    $model_data_list[$t]['Resume']['photo_tmp_path'] = $saveDirectoryTmp . DIRECTORY_SEPARATOR . md5($model_data_list[$t]['Resume']['photo_path_src']) . '.' . $file_ext;

                    // check is file already uploaded
                    if (file_exists($model_data_list[$t]['Resume']['photo_tmp_path'])) {
                        continue; // file already uploaded
                    }

                    $chs[$t] = curl_init();
                    curl_setopt($chs[$t], CURLOPT_URL, $model_data_list[$t]['Resume']['photo_path_src']);
                    curl_setopt($chs[$t], CURLOPT_RETURNTRANSFER, 1);
                    curl_multi_add_handle($cmh, $chs[$t]);
                }

                $running = null;
                do {
                    curl_multi_exec($cmh, $running);
                } while ($running > 0);

                for ($t = 0; $t < count($model_data_list); $t++) {
                    if (empty($chs[$t])) {
                        continue;
                    }

                    try { // supress file 404 errors
                        $tmp_file_name = tempnam(sys_get_temp_dir(), 'VC_IMG');
                        file_put_contents($tmp_file_name, curl_multi_getcontent($chs[$t]));
                        $data = file_get_contents($tmp_file_name);
                        $imagine = Imagine::getImagine();
                        $image = $imagine->load($data);
                        Imagine::resize($image, 800, 600)
                            ->save($model_data_list[$t]['Resume']['photo_tmp_path'], ['quality' => 90]);

                        curl_multi_remove_handle($cmh, $chs[$t]);
                        curl_close($chs[$t]);
                    } catch (\Throwable $e) {
                        unset($model_data_list[$t]['Resume']['photo_path_src']);
                        unset($model_data_list[$t]['Resume']['photo_tmp_path']);
                    }
                }
                curl_multi_close($cmh);
            }
            // --

            // --- process data to build models
            foreach ($model_data_list as $row_index => $model_data) {
                //! BUG, need register all phone numbers, - get only first phone number,
                $phone = null;
                if (!empty($model_data['Resume']['phone'])) {
                    $phone_list = explode(';', $model_data['Resume']['phone']);
                    $phone = array_shift($phone_list);
                }

                // try to find Profile id by phone number
                if (empty($phone)) {
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => [[
                            'Can\'t create resume without contact phone'
                        ]],
                        'data' => $model_data
                    ];
                    continue; // go next row
                }

                $user_phone_model = UserPhone::find()->where(['phone' => $phone])->one();
                if (empty($user_phone_model)) { // account not exists
                    // create new account and profile
                    $model_data['User'] = [
                        'login' => $phone,
                        'username' => $phone,
                        'email' => $phone . '@unknown.host',
                    ];

                    $model_data['UserPhone'] = [
                        'verified' => UserPhone::VERIFIED_INSERTED_BY_PARSER,
                        'phone' => $phone
                    ];

                    $model_data['Profile'] = [
                        'first_name' => empty($model_data['Resume']['first_name']) ? null : $model_data['Resume']['first_name'],
                        'last_name' => empty($model_data['Resume']['last_name']) ? null : $model_data['Resume']['last_name'],
                        'middle_name' => empty($model_data['Resume']['middle_name']) ? null : $model_data['Resume']['middle_name'],
                        'email' => $model_data['User']['email'], //! unknown email
                        'birth_day' => empty($model_data['Resume']['birth_day']) ? null : $model_data['Resume']['birth_day'],
                        'gender_list' => empty($model_data['Resume']['gender_list']) ? null : $model_data['Resume']['gender_list'],
                        'country_name' => empty($model_data['Resume']['country_name']) ? null : $model_data['Resume']['country_name'],
                        'country_city_id' => empty($model_data['Resume']['country_city_id']) ? 6 : $model_data['Resume']['country_city_id'],
                        'photo_tmp_path' => empty($model_data['Resume']['photo_tmp_path']) ? null : $model_data['Resume']['photo_tmp_path'],
                        'phone' => empty($model_data['Resume']['phone']) ? null : $model_data['Resume']['phone'],
                    ];
                } else if (!empty($user_phone_model->user->company)) { // this is worker's account
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => [[
                            'Not acceptable. Phone number ' . $phone . ' already registred as company and can\'t create Resume'
                        ]],
                        'data' => $model_data
                    ];
                    continue; // go next row
                } else { // profile founded and exists
                    $profile_model = $user_phone_model->user->profile;
                    if (empty($profile_model)) { // user do not complete registration, just create profile for this account
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => [[
                                'Not acceptable. Phone number ' . $phone . ' already registred user do not complete registration and can\'t create Resume'
                            ]],
                            'data' => $model_data
                        ];
                        continue; // go next row
                    }

                    $model_data['Resume']['user_id'] = $profile_model->user_id;
                }

                $model_item = new Resume();

                // save all data into DB
                $db = $model_item->getDb();
                $trans = $db->beginTransaction();

                if (empty($model_data['Resume']['user_id'])) {
                    // lets create new account
                    $model_profile = new Profile();
                    $model_user = new User();
                    $model_user_phone = new UserPhone();

                    $model_profile->loadDefaultValues();
                    $model_user->loadDefaultValues();
                    $model_user_phone->loadDefaultValues();

                    $model_profile->load($model_data);
                    $model_user->load($model_data);
                    $model_user_phone->load($model_data);

                    if (!$model_user->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_user->getErrors(),
                            'data' => $model_user->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_profile->user_id = $model_user->id;
                    if (!$model_profile->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_profile->getErrors(),
                            'data' => $model_profile->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_user_phone->user_id = $model_user->id;
                    if (!$model_user_phone->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_user_phone->getErrors(),
                            'data' => $model_user_phone->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_data['Resume']['user_id'] = $model_user->id;
                }

                // laod all data
                $model_item->loadDefaultValues();
                $model_item->loadAll([
                    'Resume' => $model_data['Resume']
                ]);

                // validate model
                if ($model_item->saveAll()) {
                    if ($save_data) {
                        //! BUG real save not working as emulated, imported just one from 11 records
                        $trans->commit();
                    } else {
                        $trans->rollBack();
                    }

                    $parse_result[$row_index] = [
                        'success' => true,
                        'data' => $model_item->getAttributes(), // $model_item->getAttributesWithRelatedAsPost()
                    ];
                } else {
                    $trans->rollBack();
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => $model_item->getErrors(),
                        'data' => $model_item->getAttributes()
                    ];
                }
            }
        }

        return $parse_result;
    }

    protected function getAttributeNameByLabel($name)
    {
        $name = trim(mb_strtolower($name));
        switch ($name) {
            case 'добавлено':
                return 'creation_time';
            case 'изменено':
                return 'update_time';
            case 'фамилия':
                return 'last_name';
            case 'имя':
                return 'first_name';
            case 'отчество':
                return 'middle_name';
            case 'дата рождения':
                return 'birth_day';
            case 'город':
                return 'country_city_id';
            case 'страна':
                return 'country_name';
            case 'образование':
                return 'resumeEducations'; // mutiplie columns support
            case 'телефон':
                return 'phone';
            case 'специализация':
                return 'title';
            case 'категория':
                return 'resumeCategoryJobs';
            // case 'место работы':        return ''; // ???
            case 'переезд':
                return 'relocation_possible';
            case 'оплата за час':
                return 'desired_salary_per_hour'; // duplicate header
            case 'оплата за месяц':
                return 'desired_salary';
            case 'валюта':
                return 'desired_salary_currency_code';
            // case 'занятость':        return '';
            case 'полный текст':
                return 'full_import_description';
            case 'текст':
                return 'full_import_description_cleaned';
            case 'опыт работы':
                return 'resumeJobs';
            case 'знание языков':
                return 'resumeLanguages';
            // case 'дополнительно':         return '';
            case 'ссылка на фото':
                return 'photo_path';
            case 'cсылка на обьявление':
                return 'source_url';
            case 'сфера':
                return 'categoryResumes';
            // +
            case 'пол':
                return 'gender_list';
            case 'желаемая страна работы':
                return 'desired_country_of_work';
            default:
                return null;
        }
    }

    protected function getReferenceInfo($reference_name)
    {
        switch ($reference_name) {
            case 'country_list':
                $data_list = ReferenceHelper::getCountryList(true);
                return [
                    'name' => 'Страны',
                    'data_list' => $data_list,
                    'data_field_list' => array_keys($data_list[0]),
                ];
            case 'currency_list':
                $data_list = ReferenceHelper::getCurrencyList();
                return [
                    'name' => 'Валюты',
                    'data_list' => $data_list,
                    'data_field_list' => array_keys($data_list[0]),
                ];
            case 'language_list':
                $data_list = ReferenceHelper::getLanguageList();
                return [
                    'name' => 'Языки',
                    'data_list' => $data_list,
                    'data_field_list' => array_keys($data_list[0]),
                ];
            default:
                throw new NotFoundHttpException('The reference name does not exist.');
        }
    }

    protected function setReferenceInfo($reference_name, $data)
    {
        switch ($reference_name) {
            case 'country_list':
                return ReferenceHelper::setCountryList($data);
            case 'currency_list':
                // check is currency code supported
                $courses_list = CurrencyConverterHelper::getCoursesList();
                foreach ($data as $data_item) {
                    $founded = false;
                    foreach ($courses_list as $currency_item) {
                        if ($data_item['char_code'] == $currency_item['CharCode']) {
                            $founded = true;
                            break;
                        }
                    }

                    if (!$founded) {
                        if ($data_item['char_code'] == 'RUR') continue; // skip RUR

                        throw new BadRequestHttpException('Unsupported currency code: ' . $data_item['char_code']);
                    }
                }

                return ReferenceHelper::setCurrencyList($data);
            case 'language_list':
                return ReferenceHelper::setLanguageList($data);
            default:
                throw new NotFoundHttpException('The reference name does not exist.');
        }
    }

}
