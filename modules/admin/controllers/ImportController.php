<?php

namespace app\modules\admin\controllers;

use app\models\Profile;
use app\models\Resume;
use Yii;
use app\models\Vacancy;
use app\models\VacancyImage;
use app\models\User;
use app\models\VacancySearch;
use app\models\Category;
use app\models\UploadExcelForm;
use app\models\CategoryJob;
use app\models\Company;
use app\models\CountryCity;
use app\models\UserPhone;
use app\models\CategoryVacancy;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\CurrencyConverterHelper;
use app\components\ReferenceHelper;
use yii\helpers\BaseFileHelper;
use yii\imagine\Image as Imagine;

/**
 * VacancyController implements the CRUD actions for Vacancy model.
 */
class ImportController extends Controller
{
    // cache attributes
    protected $country_list;
    protected $city_list;
    protected $category_list;
    protected $job_list;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            // ---
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [ // add access control by `role`
                    'class' => 'app\components\AccessRule'
                ],
                'rules' => [
                    // [
                    // 'actions' => ['index'],
                    // 'allow' => false,
                    // 'roles' => ['?', '@'],
                    // ],
                    [
                        'actions' => ['index', 'import'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Vacancy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VacancySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vacancy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionImport()
    {
        $model = new UploadExcelForm();

        if (Yii::$app->request->isPost) {
            $post_data = Yii::$app->request->post();

            if (!empty($post_data['file_path'])) {
                $model->file_path = $post_data['file_path']; //! unsafe data need filter

                // save uploaded file
                $data_arr = $model->processExcel();
                $parse_result = $this->processData($data_arr, true);
            } else {
                $model->excel_file = UploadedFile::getInstance($model, 'excel_file');
                if (!$model->upload()) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return [
                        'success' => false,
                        'errors' => $model->getErrors()
                    ];
                }

                // process uploaded file
                $data_arr = $model->processExcel();
                $parse_result = $this->processData($data_arr, false);
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'success' => true,
                'data' => [
                    'file_path' => $model->file_path,
                    'parse_result' => $parse_result,
                ]
            ];
        }

        // import data from excel file
        return $this->render('import', [
            'model' => $model
        ]);
    }

    protected function processData($data_arr, $save_data)
    {
        $parse_result = [];

        if (!empty($data_arr[0])) {
            $headers = $data_arr[0];
            $model_attribute_index = [];

            // compare header names with model data
            foreach ($headers as $index => $header) {
                $attribute_name = $this->getAttributeNameByLabel($header);
                if ($attribute_name !== null) {
                    $model_attribute_index[$index] = $attribute_name;
                }
            }

            $model_data_list = [];

            $saveDirectoryTmp = Yii::getAlias(Resume::TMP_IMAGE_DIR_ALIAS);
            if (!BaseFileHelper::createDirectory($saveDirectoryTmp, $mode = 0775)) {
                throw new HttpException(500, Yii::t('vacancy', 'Не могу создать каталог на сервере: ' . $saveDirectoryTmp));
            }

            foreach ($data_arr as $i => $item) {
                if ($i == 0) continue; // skip headers
                $model_data = [];

                foreach ($model_attribute_index as $index => $attribute) {
                    if (!isset($item[$index])) { // skip empty fields
                        continue;
                    }

                    switch ($attribute) {
                        case 'creation_time':
                            $model_data['Resume'][$attribute] = empty(date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]))) ? "null" : Yii::$app->formatter->asTimestamp($item[$index]);
                            break;
                        case 'update_time':
                            $model_data['Resume'][$attribute] = empty(date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]))) ? "null" : Yii::$app->formatter->asTimestamp($item[$index]);
                            break;
                        case 'birth_day':
                            $model_data['Resume'][$attribute] = empty(date('Y-m-d', Yii::$app->formatter->asTimestamp($item[$index]))) ? "null" : Yii::$app->formatter->asTimestamp($item[$index]);
                            break;
                        case 'country_city_id':
                            $model_data['Resume'][$attribute] = $this->getCityIdByLabel($item[$index]);
                            break;
                        case 'country_name':
                            $model_data['Resume'][$attribute] = $this->getCountryCodeByLabel($item[$index]);
                            break;
                        case 'desired_country_of_work':
                            $country_list = $this->getCountryCodesByLabel($item[$index]);
                            if (!empty($country_list)) {
                                $model_data['Resume'][$attribute] = $country_list;
                            }
                            break;
                        case 'phone':
                            $contact_phone = explode(',', $item[$index]);
                            foreach ($contact_phone as &$phone) {
                                $phone = trim($phone);
                            }
                            unset($phone);

                            if (!empty($contact_phone)) {
                                $model_data['Resume'][$attribute] = implode($contact_phone, ';');
                            }
                            break;
                        case 'relocation_possible':
                            if ($item[$index] == 'возможен переезд') {
                                $model_data['Resume'][$attribute] = Resume::RELOCATION_POSSIBLE_YES;
                            } // else default value 20
                            break;
                        case 'photo_path':
                            $model_data['Resume']['photo_path_src'] = $item[$index]; // will load from remote server
                            break;
                        case 'gender_list':
                            $model_data['Resume'][$attribute] = $this->getGendersByLabel($item[$index]);
                            break;
                        // -- relations:
                        case 'resumeLanguages':
                            $model_data['Resume']['language'] = $item[$index];
                            $model_data['Resume']['use_language'] = Resume::USE_LANGUAGE_YES;
                            $model_data['Resume']['resumeLanguages'] = $this->getResumeLanguagesByLabel($item[$index]);

                            // if data recognized then use relation
                            if (!empty($model_data['Resume']['resumeLanguages'])) {
                                $model_data['Resume']['use_language'] = Resume::USE_LANGUAGE_REALTION;
                            }
                            break;
                        case 'resumeEducations':
                            // filer html
                            $model_data['Resume']['resumeEducations'][] = [
                                'description' => str_replace("_x000D_", " ", $item[$index])
                            ];
                            break;
                        case 'resumeJobs':
                            $model_data['Resume']['job_experience'] = empty($item[$index]) ? null : str_replace("_x000D_", " ", $item[$index]);
                            $model_data['Resume']['use_job_experience'] = $model_data['Resume']['full_import_description'];
                            break;
                        case 'title':
                            $model_data['Resume']['title'] = $item[$index];
                            if (empty($model_data['Resume']['use_title'])) { // first cycle
                                $model_data['Resume']['use_title'] = Resume::USE_TITLE_YES;
                            }
                        //!!! not break;
                        case 'resumeCategoryJobs':
                            $job_list = explode(',', $item[$index]);
                            foreach ($job_list as $job_label) {
                                $model_data['Resume']['resumeCategoryJobs'][] = [
                                    'category_job_id' => $this->getCategoryJobByLabel($job_label)
                                ];
                            }

                            // if data recognized then use relation
                            if (!empty($model_data['Resume']['resumeCategoryJobs'])) {
                                $model_data['Resume']['use_title'] = Resume::USE_TITLE_REALTION;
                            }
                            break;
                        case 'categoryResumes':
                            $category_id = $this->getCategoryByLabel($item[$index]);
                            if ($category_id !== null) {
                                $model_data['Resume']['categoryVacancies'] = ['category_id' => $category_id];
                            }
                            break;
                        default:
                            $model_data['Resume'][$attribute] = $item[$index];
                            break;
                    }
                }

                // clean description
                if (empty($model_data['Resume']['full_import_description_cleaned'])) {
                    //! BUG, need sanitize html code
                    $model_data['Resume']['full_import_description_cleaned'] = $model_data['Resume']['full_import_description'];
                }

                // clear duplicates resumeCategoryJobs
                if (!empty($model_data['Resume']['resumeCategoryJobs'])) {
                    $resumeCategoryJobs = [];
                    foreach ($model_data['Resume']['resumeCategoryJobs'] as $item) {
                        $funded = false;
                        foreach ($resumeCategoryJobs as $selected_item) {
                            if ($selected_item['category_job_id'] == $item['category_job_id']) {
                                $funded = true;
                            }
                        }

                        if (!$funded) {
                            $resumeCategoryJobs[] = $item;
                        }
                    }
                    $model_data['Resume']['resumeCategoryJobs'] = $resumeCategoryJobs;
                }

                // !BUG, empty data
                if (empty($model_data['Resume']['email'])) {
                    $model_data['Resume']['email'] = '-';
                }

                $model_data_list[] = $model_data;
            }
            if ($save_data) {
                $chs = array();
                $cmh = curl_multi_init();
                for ($t = 0; $t < count($model_data_list); $t++) {
                    if (empty($model_data_list[$t]['Resume']['photo_path_src'])) {
                        continue;
                    }

                    // setup tmp file path
                    $tmp = explode('.', $model_data_list[$t]['Resume']['photo_path_src']);
                    $file_ext = array_pop($tmp);
                    $tmp = explode('/', implode('.', $tmp));
                    $file_name = array_pop($tmp);
                    $model_data_list[$t]['Resume']['photo_tmp_path'] = $saveDirectoryTmp . DIRECTORY_SEPARATOR . md5($model_data_list[$t]['Resume']['photo_path_src']) . '.' . $file_ext;

                    // check is file already uploaded
                    if (file_exists($model_data_list[$t]['Resume']['photo_tmp_path'])) {
                        continue; // file already uploaded
                    }

                    $chs[$t] = curl_init();
                    curl_setopt($chs[$t], CURLOPT_URL, $model_data_list[$t]['Resume']['photo_path_src']);
                    curl_setopt($chs[$t], CURLOPT_RETURNTRANSFER, 1);
                    curl_multi_add_handle($cmh, $chs[$t]);
                }

                $running = null;
                do {
                    curl_multi_exec($cmh, $running);
                } while ($running > 0);

                for ($t = 0; $t < count($model_data_list); $t++) {
                    if (empty($chs[$t])) {
                        continue;
                    }

                    try { // supress file 404 errors
                        $tmp_file_name = tempnam(sys_get_temp_dir(), 'VC_IMG');
                        file_put_contents($tmp_file_name, curl_multi_getcontent($chs[$t]));
                        $data = file_get_contents($tmp_file_name);
                        $imagine = Imagine::getImagine();
                        $image = $imagine->load($data);
                        Imagine::resize($image, 800, 600)
                            ->save($model_data_list[$t]['Resume']['photo_tmp_path'], ['quality' => 90]);

                        curl_multi_remove_handle($cmh, $chs[$t]);
                        curl_close($chs[$t]);
                    } catch (\Throwable $e) {
                        unset($model_data_list[$t]['Resume']['photo_path_src']);
                        unset($model_data_list[$t]['Resume']['photo_tmp_path']);
                    }
                }
                curl_multi_close($cmh);
            }
            // --

            // --- process data to build models
            foreach ($model_data_list as $row_index => $model_data) {
                //! BUG, need register all phone numbers, - get only first phone number,
                $phone = null;
                if (!empty($model_data['Resume']['phone'])) {
                    $phone_list = explode(';', $model_data['Resume']['phone']);
                    $phone = array_shift($phone_list);
                }

                // try to find Profile id by phone number
                if (empty($phone)) {
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => [[
                            'Can\'t create resume without contact phone'
                        ]],
                        'data' => $model_data
                    ];
                    continue; // go next row
                }

                $user_phone_model = UserPhone::find()->where(['phone' => $phone])->one();
                if (empty($user_phone_model)) { // account not exists
                    // create new account and profile
                    $model_data['User'] = [
                        'login' => $phone,
                        'username' => $phone,
                        'email' => $phone . '@unknown.host',
                    ];

                    $model_data['UserPhone'] = [
                        'verified' => UserPhone::VERIFIED_INSERTED_BY_PARSER,
                        'phone' => $phone
                    ];

                    $model_data['Profile'] = [
                        'first_name' => empty($model_data['Resume']['first_name']) ? null : $model_data['Resume']['first_name'],
                        'last_name' => empty($model_data['Resume']['last_name']) ? null : $model_data['Resume']['last_name'],
                        'middle_name' => empty($model_data['Resume']['middle_name']) ? null : $model_data['Resume']['middle_name'],
                        'email' => $model_data['User']['email'], //! unknown email
                        'birth_day' => empty($model_data['Resume']['birth_day']) ? null : $model_data['Resume']['birth_day'],
                        'gender_list' => empty($model_data['Resume']['gender_list']) ? null : $model_data['Resume']['gender_list'],
                        'country_name' => empty($model_data['Resume']['country_name']) ? null : $model_data['Resume']['country_name'],
                        'country_city_id' => empty($model_data['Resume']['country_city_id']) ? 6 : $model_data['Resume']['country_city_id'],
                        'photo_tmp_path' => empty($model_data['Resume']['photo_tmp_path']) ? null : $model_data['Resume']['photo_tmp_path'],
                        'phone' => empty($model_data['Resume']['phone']) ? null : $model_data['Resume']['phone'],
                    ];
                } else if (!empty($user_phone_model->user->company)) { // this is worker's account
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => [[
                            'Not acceptable. Phone number ' . $phone . ' already registred as company and can\'t create Resume'
                        ]],
                        'data' => $model_data
                    ];
                    continue; // go next row
                } else { // profile founded and exists
                    $profile_model = $user_phone_model->user->profile;
                    if (empty($profile_model)) { // user do not complete registration, just create profile for this account
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => [[
                                'Not acceptable. Phone number ' . $phone . ' already registred user do not complete registration and can\'t create Resume'
                            ]],
                            'data' => $model_data
                        ];
                        continue; // go next row
                    }

                    $model_data['Resume']['user_id'] = $profile_model->user_id;
                }

                $model_item = new Resume();

                // save all data into DB
                $db = $model_item->getDb();
                $trans = $db->beginTransaction();

                if (empty($model_data['Resume']['user_id'])) {
                    // lets create new account
                    $model_profile = new Profile();
                    $model_user = new User();
                    $model_user_phone = new UserPhone();

                    $model_profile->loadDefaultValues();
                    $model_user->loadDefaultValues();
                    $model_user_phone->loadDefaultValues();

                    $model_profile->load($model_data);
                    $model_user->load($model_data);
                    $model_user_phone->load($model_data);

                    if (!$model_user->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_user->getErrors(),
                            'data' => $model_user->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_profile->user_id = $model_user->id;
                    if (!$model_profile->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_profile->getErrors(),
                            'data' => $model_profile->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_user_phone->user_id = $model_user->id;
                    if (!$model_user_phone->save()) {
                        $trans->rollBack();
                        $parse_result[$row_index] = [
                            'success' => false,
                            'errors' => $model_user_phone->getErrors(),
                            'data' => $model_user_phone->getAttributes()
                        ];
                        continue; // go next row
                    }

                    $model_data['Resume']['user_id'] = $model_user->id;
                }

                // laod all data
                $model_item->loadDefaultValues();
                $model_item->loadAll([
                    'Resume' => $model_data['Resume']
                ]);

                // validate model
                if ($model_item->saveAll()) {
                    if ($save_data) {
                        //! BUG real save not working as emulated, imported just one from 11 records
                        $trans->commit();
                    } else {
                        $trans->rollBack();
                    }

                    $parse_result[$row_index] = [
                        'success' => true,
                        'data' => $model_item->getAttributes(), // $model_item->getAttributesWithRelatedAsPost()
                    ];
                } else {
                    $trans->rollBack();
                    $parse_result[$row_index] = [
                        'success' => false,
                        'errors' => $model_item->getErrors(),
                        'data' => $model_item->getAttributes()
                    ];
                }
            }
        }

        return $parse_result;
    }

    //$data = $data;
    //$category = ""; "category";
    //$original = "";
    //$en = "";   "en";
    //$ru = "";   "ru";
    //$ua = "";   "ua";

    protected function getAttributeNameByLabel($name)
    {
        $name = trim(mb_strtolower($name));
        switch ($name) {
            case 'Без перевода':
                return 'original';
            case 'english':
                return 'en';
            case 'russian':
                return 'ru';
            case 'ukrainian':
                return 'ua';
            default:
                return null;
        }
    }

    /**
     * Creates a new Vacancy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vacancy();
        $model->loadDefaultValues();

        if (Yii::$app->request->isPost) {
            $post_data = Yii::$app->request->post();
            // $post_data['Vacancy']['company_id'] = Yii::$app->user->identity->company->id;
            $post_data['Vacancy']['creation_time'] = time();
            $post_data['Vacancy']['update_time'] = time();
            if (empty($post_data['Vacancy']['salary_per_hour_min'])) {
                return $post_data['Vacancy']['salary_per_hour_min'] = "5";
            }

            $post_data['Vacancy']['salary_per_hour_min_src'] = CurrencyConverterHelper::currencyToCurrency(
                $post_data['Vacancy']['salary_per_hour_min'],
                $post_data['Vacancy']['currency_code'],
                Yii::$app->params['sourceCurrencyCharCode']
            );

            if (empty($post_data['Vacancy']['salary_per_hour_min_src'])) {
                $post_data['Vacancy']['salary_per_hour_min_src'] = 9;
            }

            $post_data['Vacancy']['salary_per_hour_max_src'] = CurrencyConverterHelper::currencyToCurrency(
                $post_data['Vacancy']['salary_per_hour_max'],
                $post_data['Vacancy']['currency_code'],
                Yii::$app->params['sourceCurrencyCharCode']
            );

            // unlink deleted relations
            if (!empty($post_data['relations'])) {
                foreach ($post_data['relations'] as $relation_name) {
                    //! BUG of optimization need save `id` for unmodifed relations
                    $model->unlinkAll($relation_name, true);
                }
            }

            if ($model->loadAll($post_data) && $model->saveAll()) {
                // set main_image, just get first
                // if(!empty($model->vacancyImages)) {
                //     $model->main_image = $model->vacancyImages[0]->path_name;
                //     $model->save(false);
                // }

                return $this->redirect(['view', 'id' => $model->id]);
            } else if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'success' => false,
                    'errors' => $model->getErrors()
                ];
            }
        }

        // default values
        $model->currency_code = Yii::$app->params['defaultCurrencyCharCode'];
        $model->residence_amount_currency_code = Yii::$app->params['defaultCurrencyCharCode'];
        $model->agency_paid_document_currency_code = Yii::$app->params['defaultCurrencyCharCode'];
        $model->agency_pay_commission_currency_code = Yii::$app->params['defaultCurrencyCharCode'];

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDeleted()
    {
        $resume = Vacancy::find()->where('id != :id ', ['id' => 1])->all();
        foreach ($resume as $model) {
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Vacancy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $post_data = Yii::$app->request->post();
            $post_data['Vacancy']['company_id'] = Yii::$app->user->identity->company->id;
            $post_data['Vacancy']['update_time'] = time();

            $post_data['Vacancy']['salary_per_hour_min_src'] = CurrencyConverterHelper::currencyToCurrency(
                $post_data['Vacancy']['salary_per_hour_min'],
                $post_data['Vacancy']['currency_code'],
                Yii::$app->params['sourceCurrencyCharCode']
            );
            $post_data['Vacancy']['salary_per_hour_max_src'] = CurrencyConverterHelper::currencyToCurrency(
                $post_data['Vacancy']['salary_per_hour_max'],
                $post_data['Vacancy']['currency_code'],
                Yii::$app->params['sourceCurrencyCharCode']
            );

            // unlink deleted relations
            if (!empty($post_data['relations'])) {
                foreach ($post_data['relations'] as $relation_name) {
                    //! BUG of optimization need save `id` for unmodifed relations
                    $model->unlinkAll($relation_name, true);
                }
            }

            if ($model->loadAll($post_data) && $model->saveAll()) {
                // set main_image, just get first
                // if(!empty($model->vacancyImages)) {
                //     $model->main_image = $model->vacancyImages[0]->path_name;
                //     $model->save(false);
                // }

                return $this->redirect(['view', 'id' => $model->id]);
            } else if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'success' => false,
                    'errors' => $model->getErrors()
                ];
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vacancy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Export Vacancy search result into Excel(*.xls).
     * @return file|mixed
     */
    public function actionExport()
    {
        // export data to excel file
        return $this->render('export');
    }

    /**
     * Import Vacancy Excel(*.xls) into DB
     * @return file|mixed
     */

    protected function getAttributeNameByLabel($name)
    {
        $name = trim(mb_strtolower($name));
        switch ($name) {
            case 'вакансия':
                return 'title';
            case 'зарплата мин в час':
                return 'salary_per_hour_min';
            case 'зарплата макс в час':
                return 'salary_per_hour_max';
            case 'зарплата мин':
            case 'зарплата, от':
                return 'prepaid_expense_min';
            case 'зарплата макс':
            case 'зарплата, до':
                return 'prepaid_expense_max';
            case 'валюта':
                return 'currency_code';
            case 'текст':
                return 'full_import_description';
            case 'ред. текст':
                return 'full_import_description_cleaned';
            case 'описание вакансии':
                return 'job_description';
            case 'сфера':
                return 'categoryVacancies';
            case 'категория':
                return 'category_job_id';
            case 'телефон':
                return 'contact_phone';
            case 'дата':
                return 'creation_time';
            case 'занятость':
                return 'employment_type';
            case 'работодатель':
                return 'company_name';
            case 'предприятие':
                return 'vacancy_company_name';
            case 'email компании':
                return 'company_email';
            case 'телефон компании':
                return 'company_phone';
            case 'страна компании':
                return 'company_country_code';
            case 'город местонахождения компании':
                return 'company_country_city_id';
            case 'компания подтверждена':
                return 'company_verification';
            case 'сайт':
                return 'site';
            case 'имя сотрудника':
                return 'company_worker_name';
            case 'email сотрудника':
                return 'company_worker_email';
            case 'телефон администратора':
                return 'contact_phone_for_admin';
            case 'для граждан таких стран':
                return 'worker_country_codes';
            case 'страна работы':
                return 'country_name';
            case 'город работы':
                return 'country_city_id';
            case 'тип':
                return 'type';
            case 'логотип':
                return 'logo';
            case 'ссылка':
                return 'source_url';
            case 'пол':
                return 'gender_list';
            case 'возраст от':
                return 'age_min';
            case 'возраст до':
                return 'age_max';
            case 'свободных мест':
                return 'free_places';
            case 'свободный заезд':
                return 'date_free';
            case 'дата заезда от':
                return 'date_start';
            case 'дата заезда до':
                return 'date_end';
            case 'занятость в день от':
                return 'hours_per_day_min';
            case 'занятость в день до':
                return 'hours_per_day_max';
            case 'Занятость дней в неделю от':
                return 'days_per_week_min';
            case 'Занятость дней в неделю до':
                return 'days_per_week_max';
            case 'проживание предоставляется':
                return 'residence_provided';
            case 'проживание в месяц':
                return 'residence_amount';
            case 'проживание валюта':
                return 'residence_amount_currency_code';
            case 'проживание человек в комнате':
                return 'residence_people_per_room';
            case 'документы прелоставляются':
                return 'documents_provided';
            case 'требуемые документы от кандидата':
                return 'documents_required';
            case 'email адреса':
                return 'contact_email_list';
            case 'контактные телефоны':
                return 'contact_phone';
            case 'контактное лицо':
                return 'contact_name';
            default:
                return null;
        }
    }

    protected function getCountryCodeByLabel($country_name)
    {
        if (empty($this->country_list)) {
            $this->country_list = ReferenceHelper::getCountryList(true);
        }

        $country_name = mb_strtolower($country_name);
        foreach ($this->country_list as $country) {
            // search by all locale labels
            foreach (Yii::$app->components['urlManager']['languages'] as $lang) {
                $country_tr = mb_strtolower(Yii::t('country', $country['name'], [], $lang));
                if ($country_tr == $country_name) {
                    return $country['char_code'];
                }
            }
        }

        return null;
    }

    protected function getCountryCodesByLabel($country_names)
    {
        if (empty($this->country_list)) {
            $this->country_list = ReferenceHelper::getCountryList(true);
        }

        $country_name_list = explode(',', $country_names);
        foreach ($country_name_list as &$val) {
            $val = trim(mb_strtolower($val));
        }

        $selected_list = [];
        foreach ($this->country_list as $country) {
            // search by all locale labels
            foreach ($country_name_list as $country_name) {
                foreach (Yii::$app->components['urlManager']['languages'] as $lang) {
                    $country_tr = mb_strtolower(Yii::t('country', $country['name'], [], $lang));
                    if ($country_tr == $country_name) {
                        $selected_list[] = $country['char_code'];
                    }
                }
            }
        }

        $selected_list = array_unique($selected_list);

        if (empty($selected_employment_type_list))
            return null;

        return implode($selected_list, ';') . ';';
    }

    protected function getCityIdByLabel($city_name)
    {
        if (empty($this->city_list)) {
            $this->city_list = CountryCity::find()->all(); //! BUG of optimization, need redis cache
        }

        $city_name = mb_strtolower($city_name);
        foreach ($this->city_list as $city) {
            // search by all locale labels
            foreach (Yii::$app->components['urlManager']['languages'] as $lang) {
                $city_tr = mb_strtolower(Yii::t('city', $city->city_name, [], $lang));
                if ($city_tr == $city_name) {
                    return $city->id;
                }
            }
        }

        return null;
    }

    protected function getCategoryByLabel($category_name)
    {
        if (empty($this->category_list)) {
            $this->category_list = Category::getUserSelectList();
        }
        $category_name = mb_strtolower($category_name);
        foreach ($this->category_list as $category) {
            foreach (Yii::$app->components['urlManager']['languages'] as $lang) {
                if (mb_strtolower(Yii::t('category', $category->name, [], $lang)) == $category_name) {
                    return $category->id;
                }
            }
        }

        return null;
    }

    protected function getCategoryJobByLabel($job_name)
    {
        if (empty($this->job_list)) {
            $this->job_list = CategoryJob::getUserMultiSelectList();
        }
        $job_name = trim(mb_strtolower($job_name));
        foreach ($this->job_list as $job_group) {
            foreach ($job_group['jobs'] as $job_item) {
                // search by all locale labels
                foreach (Yii::$app->components['urlManager']['languages'] as $lang) {
                    if (mb_strtolower(Yii::t('category-job', $job_item['name'], [], $lang)) == $job_name) {
                        return $job_item['id'];
                    }
                }
            }
        }

        return null;
    }

    protected function getEmploymentTypeByLabel($employment_type)
    {
        $employment_list_aliases = [
            Vacancy::EMPLOYMENT_TYPE_FULL_TIME => [
                'полная занятость'
            ],
            Vacancy::EMPLOYMENT_TYPE_SHIFT_METHOD => [

            ],
            Vacancy::EMPLOYMENT_TYPE_PART_TIME => [
                'временная занятость',
                'частичная занятость'
            ],
            // Vacancy::EMPLOYMENT_TYPE_SHIFT_WORK   => Yii::t('vacancy', 'Shift work'),
        ];

        $employment_type_arr = explode(',', $employment_type);

        // remove spaces
        foreach ($employment_type_arr as &$val) {
            $val = trim(mb_strtolower($val));
        }

        $selected_employment_type_list = [];
        foreach ($employment_type_arr as $employment_type_label) {
            foreach ($employment_list_aliases as $ukey => $aliases) {
                foreach ($aliases as $alias) {
                    if ($alias == $employment_type_label) {
                        $selected_employment_type_list[] = $ukey;
                    }
                }
            }
        }

        // clear duplicates
        $selected_employment_type_list = array_unique($selected_employment_type_list);

        if (empty($selected_employment_type_list))
            return null;

        return implode($selected_employment_type_list, ';') . ';';
    }

    protected function getGendersByLabel($gender_list)
    {
        $gender_list_aliases = [
            Vacancy::GENDER_MALE => [
                'м',
                'муж',
                'мужчина'
            ],
            Vacancy::GENDER_FEMALE => [
                'ж',
                'жен',
                'женщина',
            ],
            Vacancy::GENDER_PAIR => [
                'п',
                'пара'
            ]
        ];

        $gender_list_arr = explode(',', $gender_list);

        // remove spaces
        foreach ($gender_list_arr as &$val) {
            $val = trim(mb_strtolower($val));
        }

        $selected_gender_list = [];
        foreach ($gender_list_arr as $gender_list_label) {
            foreach ($gender_list_aliases as $ukey => $aliases) {
                foreach ($aliases as $alias) {
                    if ($alias == $gender_list_label) {
                        $selected_gender_list[] = $ukey;
                    }
                }
            }
        }

        // clear duplicates
        $selected_gender_list = array_unique($selected_gender_list);

        if (empty($selected_gender_list))
            return null;

        return implode($selected_gender_list, ';') . ';';
    }

    protected function getCompanyStatusByLabel($status)
    {
        if (mb_strtolower($status) == 'да') {
            return Company::STATUS_VERIFIED;
        }

        return Company::STATUS_NOT_VERIFIED;
    }

    protected function getDateFreeByLabel($status)
    {
        if (mb_strtolower($status) == 'да') {
            return Vacancy::DATE_FREE_YES;
        }

        return Vacancy::DATE_FREE_NO;
    }

    protected function getResidenceProvidedByLabel($status)
    {
        if (mb_strtolower($status) == 'да') {
            return Vacancy::RESIDENCE_PROVIDED_YES;
        }

        return Vacancy::RESIDENCE_PROVIDED_NO;
    }

    protected function getDocumentsProvidedByLabel($documents_provided)
    {
        $documents_provided_arr = explode(',', $documents_provided);

        // remove spaces
        foreach ($documents_provided_arr as &$val) {
            $val = trim(mb_strtolower($val));
        }

        $documents_provided_list = Vacancy::getDocumentsProvidedList();
        $selected_documents_provided = [];
        foreach ($documents_provided_arr as $documents_provided_label) {
            foreach ($documents_provided_list as $ukey => $alias) {
                if (mb_strtolower(Yii::t('vacancy', $alias, 'ru')) == $documents_provided_label) {
                    $selected_documents_provided[] = $ukey;
                }
            }
        }

        // clear duplicates
        $selected_documents_provided = array_unique($selected_documents_provided);

        if (empty($selected_documents_provided))
            return null;

        return implode($selected_documents_provided, ';') . ';';
    }

    protected function getDocumentsRequiredByLabel($documents_required)
    {
        $documents_required_arr = explode(',', $documents_required);

        // remove spaces
        foreach ($documents_required_arr as &$val) {
            $val = trim(mb_strtolower($val));
        }

        $documents_required_list = Vacancy::getDocumentsRequiredList();
        $selected_documents_required = [];
        foreach ($documents_required_arr as $documents_required_label) {
            foreach ($documents_required_list as $ukey => $alias) {
                if (mb_strtolower(Yii::t('vacancy', $alias, 'ru')) == $documents_required_label) {
                    $selected_documents_required[] = $ukey;
                }
            }
        }

        // clear duplicates
        $selected_documents_required = array_unique($selected_documents_required);

        if (empty($selected_documents_required))
            return null;

        return implode($selected_documents_required, ';') . ';';
    }

    protected function getCompanyTypeByLabel($type)
    {
        $type_list_aliases = [
            Company::TYPE_EMPLOYER => [
                'прямой работодатель'
            ],
            Company::TYPE_HR_AGENCY => [
                'кадровое агентство'
            ],
        ];

        foreach ($type_list_aliases as $ukey => $aliases) {
            foreach ($aliases as $aliase) {
                if ($aliase == $type)
                    return $ukey;
            }
        }

        return null;
    }

    protected function findModel($id)
    {
        if (($model = Vacancy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('vacancy', 'The requested page does not exist.'));
    }
}

