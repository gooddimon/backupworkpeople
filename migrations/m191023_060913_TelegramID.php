<?php

use yii\db\Migration;

/**
 * Class m191023_060913_userpreferences
 */
class m191023_060913_TelegramID extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%TelegramID}}', [
            'id' => $this->primaryKey(),
            'userpreferences' => $this->json()->notNull(),
            'created_at' => $this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191023_060913_userpreferences cannot be reverted.\n";

        return false;
    }


}
